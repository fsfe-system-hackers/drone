<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe e.V. <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-only
-->

# Drone deployment

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/drone/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/drone)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/drone)

This playbook is used to deploy [drone](https://drone.io/). This is a
[Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration)
and [Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery)
software used to deploy all dockerised FSFE services.

There is one runner on the same host the Drone server is deployed to.
Additionally, there are runner on each FSFE [container
server](https://git.fsfe.org/fsfe-system-hackers/container-server/).

## How to deploy Drone?

Run the following command from the computer where your GPG private key is:

```sh
ansible-playbook playbook.yml
```

The vault passphrase [vault passphrase](vaultpw.gpg) is encrypted to some of the
FSFE's system hackers.

The file [vaultpw.sh](vaultpw.sh) decrypts the passphrase required to open the
vault.

## Configuration

Most of the configuration happens in the `drone_server` group vars. Here, you
can also enter new versions of the drone server, runner, and cli.
